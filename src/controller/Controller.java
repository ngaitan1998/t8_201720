package controller;

import java.io.File;

import model.data_structures.HashTable;
import model.exceptions.ElementNotFoundException;
import model.logic.STSManager;
import model.vo.StopVO;
import model.vo.TripVO;
import api.ISTSManager;

public class Controller {
	
	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();

	public static void loadTrips() {
		manager.loadTrips();
	}
	public static void loadST() {
		manager.loadStopTimes();
	}
	public static void loadStops() 
	{
		manager.loadStops();	
	}
	public static void tripsByStopAndHour(Integer stopId){
		manager.printTrips(stopId);
	}
	public static void stopsHourRange(Integer stopId, String initialHour, String finalHour){
		manager.stopsHourRange(stopId, initialHour, finalHour);
	}
	
}
