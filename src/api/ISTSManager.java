package api;

import java.io.File;

import model.data_structures.HashTable;
import model.data_structures.RedBlackBST;
import model.vo.StopVO;
import model.vo.TripVO;


public interface ISTSManager {

	/**
	 * Method to load the trips of the STS
	 * @param tripsFile - path to the file 
	 */
	public void loadTrips();
	
	public void loadStopTimes();

	public void loadStops();

	public RedBlackBST<TripVO, Integer> tripsByStopAndHour(Integer stopId);
	
	public void printTrips(Integer stopId);
	
	public void stopsHourRange(Integer stopId, String initialHour, String finalHour);


//	public HashTable<TripVO, Integer> getTripsById(Integer stopId);
//
//	public HashTable<StopVO, Integer> getSharedStops(Integer trip1, Integer trip2);
//	
//	public void sortTrips(HashTable<TripVO, Integer> tripsToSort);
//
//	public void sortStops(HashTable<StopVO, Integer> stopsToSort);

}
