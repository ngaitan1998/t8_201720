package model.vo;

import model.data_structures.HashTable.LinearHash;

public interface IVo <V, K>
{
	LinearHash<V, K> getConexiones();
	void addConexion(K key, V val);
	K getKey();
}
