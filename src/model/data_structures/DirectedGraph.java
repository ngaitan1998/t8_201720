package model.data_structures;
import model.data_structures.HashList.HashNode;
import model.data_structures.HashTable.LinearHash;
import model.exceptions.ElementNotFoundException;
import model.vo.IVo;
public class DirectedGraph <K, V extends IVo<V, K> >
{
	LinearHash<V, K> vals;
	public DirectedGraph()
	{
		vals = new LinearHash<V, K>(31);
	}
	public void addVertex(K id, V objeto)
	{
		vals.put(id, objeto);
	}
	public void addEdge( K origen, K destino, double peso) throws ElementNotFoundException
	{
		 V orig = vals.get(origen);
		 V fin = vals.get(destino);
		 if( orig != null && fin != null )
		 {
			 orig.addConexion(fin.getKey(), fin);
			 fin.addConexion(orig.getKey(), orig);
		 }
		 else
		 {
			 throw new ElementNotFoundException("Fallo");
		 }
	}
	public V get(K key)
	{
		return vals.get(key);
	}
	//Esto hace lo mismo que el CC
	public StructuresIterator<V> iterator()
	{
		return vals.elementsIterator();
	}
	//Esto verifica lo de los ciclos
	public boolean hayCiclo()
	{
		return false;
	}
}
