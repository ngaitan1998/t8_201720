package model.data_structures;


public class List < T extends Comparable<T> > implements IList< T >
{
	Node<T> first;

	Node<T> last;
	
	Ordenador<T>.HeapSort sort = new Ordenador<T>().new HeapSort();

	public List()
	{
		first = null;
		last = null;
		
	}
	
	
	public Node<T> getFirstNode()
	{
		
		return first;
	}
	
	public Node<T> getLastNode()
	{
		return last;
	}
	
	public Ordenador<T>.HeapSort getComputer(){
		return sort;
	}
	

    public Object[] toArray( )
    {
        Object[] array = new Object[getSize()];
        Node<T> n  = first;
        int pos = 0;
        while(n!= null)
        {
            array[pos] = n.getElement();
            n = n.getNext();
            pos++;
        }
        return array;
    }
    
    public <T> T[] toArray( T[] array )
    {
        if(array.length < getSize( ))
        {
            return (T[])toArray();
        }
        else
        {
            Node nodi  = first;
            int pos = 0;
            while(nodi!= null)
            {
                array[pos] = (T)nodi.getElement();
                nodi = nodi.getNext();
                pos++;
            }
            if(array.length > getSize())
            {
                array[getSize( )] = null;
            }
            return array;
        }
    }
	public void add( T elem ) 
	{
		if( elem ==  null) throw new NullPointerException();
		if( first == null )
		{
			first = new Node<T>(elem);
			last = first;
		}
		else
		{
			Node<T> node = new Node<T>(elem);

			if(getSize() == 1)
			{
				last = node;
				first.changeNext(last);
			}
			else
			{
				last.changeNext(node);
				last = node;
			}


		}
	}
	

	public void addAtK(int pos, T elem) 
	{
		Node<T> add = new Node<T>(elem);
		int size = getSize();

		if(pos > size || pos < 0) throw new IndexOutOfBoundsException();
		if(elem == null ) throw new NullPointerException();
		if( size == 0 )
		{
			first = new Node<T>(elem);
			last = first;
		}
		else if(pos == 0) 
		{ 
			add.changeNext(first);
			first = add;
		}
		else if( pos == size )
		{
			last.changeNext(add);
			last = add;
		}
		else 
		{			
			Node<T> temp = first;
			int position = 0;
			boolean added = false;
			while( !added )
			{
				if(position == pos - 1)
				{
					add.changeNext(temp.getNext());
					temp.changeNext(add);
					added = true;
				}
				temp = temp.getNext();
				position++;
			}
		}
	}

	public void deleteAtK(int pos) 
	{
		// TODO Auto-generated method stub
		int size = getSize();
		if(pos > size - 1 || pos < 0) throw new IndexOutOfBoundsException();
		if(size == 1)
		{
			first = null;
			last = null;
		}
		else if(pos == 0)
		{
			first = first.getNext();
		}
		else
		{
			int position = 1;
			boolean delete = false;
			Node<T> temp = first;
			while(!delete)
			{
				if( position == pos )
				{
					temp.changeNext(temp.getNext().getNext());
					delete = true;
					if(position == size - 1) last = temp;
				}
				position++;
				temp = temp.getNext();
			}
		}
	}

	public Integer getSize() 
	{
		Integer size = 0;
		Node<T> nodo = first;

		while( nodo != null)
		{
			size++;
			nodo = nodo.getNext( );
		}
		return size;
	}

	public T getFirst()
	{
		return first == null? null:first.getElement();
	}

	public T getLast()
	{
		return last == null? null:last.getElement();
	}

	public StructuresIterator<T> iterator() 
	{
		return new StructuresIterator<T>(first);
	}


	@Override
	public T getElementAtK(int pos) {
		// TODO Auto-generated method stub
		T search = null;
		int size = getSize();
		if(pos > size|| pos < 0) throw new IndexOutOfBoundsException();
		//else if(first == null) throw new NullPointerException();
		else if(pos == 0) search = first.getElement();
		//last -> es el primero
		else if(pos == size - 1) search  = last.getElement();
		else{
			Node<T> temp = first.getNext();
			boolean found = false;
			int i = 1;
			while(!found){
				if(i == pos){
					search = temp.getElement();
					found = true;
				}
				temp = temp.getNext();
				i++;
			}
		}
		return search;
	}

	
	public List<T> sort(T[] array){
		T[] arrSort = toArray(array);
		List <T> ordenada = new List<T>();
		sort.ordenarHeapSort(arrSort);
		for(int i = 0; i < array.length; i++){
			ordenada.add(array[i]);
		}
		return ordenada;
	}
	
	public void restart()
	{
		first = null;
	}

	public static void main(String[] args) {
		List<Integer> nums = new List<Integer>();
		nums.add(10);
		nums.add(2);
		nums.add(33);
		nums.add(14);
		nums.add(53);
		nums.add(26);
		nums.add(17);
		nums.add(8);
		nums.add(39);
		
		Integer[] arr = new Integer[nums.getSize()];
		nums = nums.sort(arr);
		for (int i = 0; i < nums.getSize(); i++) {
			System.out.print(nums.getElementAtK(i) + " ");
		}
	}
}

