package model.data_structures;

public interface HashTable <V, K >
{
	void put( K key, V val);
	V get(K key);
	int hash(K key);
	int size();
	void rehash(int cap);
	StructuresIterator<K> iterator();
	StructuresIterator<V> elementsIterator();
	boolean isEmpty();
	public class SeparateChainHash<V, K  > implements HashTable<V, K>
	{
		int n, m;
		HashList<V,K>[] hl;
		//para una nueva hashtable
		public SeparateChainHash()
		{
			this( 997 );
		}
		//para hacer aumentar la capacidad con rehash()
		public SeparateChainHash(int cap)
		{
			m = cap;
			hl = (HashList<V, K>[]) new HashList[m];
			for (int i = 0; i < m; i++)
				hl[i] = new HashList<V, K>();
		}
		@Override
		public void put(K key, V val) 
		{
			// TODO Auto-generated method stub
			HashList<V, K> h = hl[hash(key)];
			if(h.isEmpty()) n++;;
			h.put(key, val);
		}

		@Override
		public V get(K key) {
			// TODO Auto-generated method stub
			return hl[hash(key)].get(key);
		}

		@Override
		public int hash(K key) {
			// TODO Auto-generated method stub
			return ( key.hashCode() & 0x7fffffff) % m;
		}

		@Override
		public int size() {
			// TODO Auto-generated method stub
			n = 0;
			for (int i = 0; i < m; i++) 
			{
				n += hl[i].size();
			}
			return n;
		}

		@Override
		public void rehash(int cap) {
			// TODO Auto-generated method stub
			SeparateChainHash<V, K> temp = new SeparateChainHash<V, K>(cap);
			for (int i = 0; i < m; i++) {
				StructuresIterator<K> keys = hl[i].iterator();
				for ( ; keys.getCurrent() != null; keys.next()) {
					temp.put( keys.getElement(), hl[i].get(keys.getElement()));
				}
			}
			this.m  = temp.m;
			this.n  = temp.n;
			this.hl = temp.hl;
		}

		@Override
		public StructuresIterator<K> iterator() {
			// TODO Auto-generated method stub
			Queue<K> queue = new Queue<K>();
			for (int i = 0; i < m; i++) 
			{
				StructuresIterator<K> keys = hl[i].iterator();
				for ( ; keys.getCurrent() != null; keys.next()) 
				{
					queue.enqueue(keys.getElement());
				}
			}
			return queue.iterator();
		}
		@Override
		public StructuresIterator<V> elementsIterator() {
			// TODO Auto-generated method stub
			Queue<V> queue = new Queue<V>();
			for (int i = 0; i < m; i++) 
			{
				StructuresIterator<V> vals = hl[i].vals();
				for ( ; vals.getCurrent() != null; vals.next()) 
				{
					queue.enqueue(vals.getElement());
				}
			}
			return queue.iterator();		
		}
		@Override
		public boolean isEmpty() {
			// TODO Auto-generated method stub
			return size() == 0;
		}
	}
	public class LinearHash<V, K> implements HashTable<V,K>
	{
		int n, m;
		K[] keys;
		V[] vals;
		//para una nueva hashtable
		public LinearHash()
		{
			this( 97 );
		}
		//para hacer aumentar la capacidad con rehash()
		public LinearHash(int cap)
		{
			m = cap;
			keys = (K[]) new Object[m];
			vals = (V[]) new Object[m];
			n = 0;
		}
		@Override
		public void put( K key, V val ) 
		{
			// TODO Auto-generated method stub
			if (n/m >= 0.75) rehash(2*m);
			int i = hash(key);
			for ( ; keys[i] != null; i = (i + 1) % m)
			{
				if (keys[i].equals(key)) 
				{ 
					vals[i] = val; 
					return; 
				}
			}
			keys[i] = key;
			vals[i] = val;
			n++;

		}
		@Override
		public V get(K key) 
		{
			// TODO Auto-generated method stub
			for(int i = hash(key); keys[i] != null; i = (i + 1) % m)
			{
				if(keys[i].equals(key))
				{
					return vals[i];
				}
			}
			return null;
		}
		@Override
		public int hash(K key) 
		{
			return ( ( Integer ) key.hashCode() & 0x7fffffff) % m;
		}
		@Override
		public int size() {
			// TODO Auto-generated method stub
			return n;	
		}
		public void rehash(int cap)
		{
			LinearHash<V, K> t;
			t = new LinearHash<V, K>(cap);
			for (int i = 0; i < m; i++)
			{
				if (keys[i] != null) t.put( keys[i], vals[i] );
			}
			keys = (K[]) t.keys;
			vals = (V[]) t.vals;
			m  = t.m;
		}
		@Override
		public StructuresIterator<K> iterator() 
		{
			Queue<K> queue = new Queue<K>();
			for (int i = 0; i < m; i++)
				if (keys[i] != null) queue.enqueue(keys[i]);
			return queue.iterator();		
		}
		@Override
		public StructuresIterator<V> elementsIterator() {
			// TODO Auto-generated method stub
			Queue<V> queue = new Queue<V>();
			for (int i = 0; i < m; i++)
				if (keys[i] != null) queue.enqueue(vals[i]);
			return queue.iterator();			
		}
		public boolean isEmpty()
		{
			return n == 0;
		}
	}
}
