package model.logic;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;


import model.data_structures.HashTable;
import model.data_structures.List;
import model.data_structures.RedBlackBST;
import model.data_structures.StructuresIterator;
import model.vo.StopTimeVO;
import model.vo.StopVO;
import model.vo.TripVO;
import api.ISTSManager;


public class STSManager implements ISTSManager{

	HashTable<TripVO, Integer> tripsTable = new HashTable.LinearHash<TripVO, Integer>();
	HashTable<StopTimeVO, String> stopTimesTable = new HashTable.SeparateChainHash<StopTimeVO, String>();
	HashTable<StopVO, Integer> stopTable = new HashTable.LinearHash<StopVO, Integer>();

	public void loadTrips() 
	{
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/trips.txt")));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");
				int routeId = Integer.parseInt(data[0]);
				int serviceId = Integer.parseInt(data[1]);
				int tripId = Integer.parseInt(data[2]);
				String headSing = data[3];
				String tripShortName = data[4];
				String directionId = data[5];
				int blockId = Integer.parseInt(data[6]);
				String shapeId = data[7];
				int wheelchairFriendly = Integer.parseInt(data[8]);
				int bikeAllowed = Integer.parseInt(data[9]);

				TripVO nuevo = new TripVO(routeId, serviceId, tripId, headSing, 
						tripShortName, directionId, blockId, shapeId, bikeAllowed, wheelchairFriendly);
				tripsTable.put(nuevo.getKey(), nuevo);
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	public void loadStopTimes()
	{
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/stop_times.txt")));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");
				int id = Integer.parseInt(data[0]);
				String arrivalTimeData = data[1];
				String departureTimeData = data[2];
				int stopId = Integer.parseInt(data[3]);
				String stopSequence = data[4];
				String stopSign = data[5];
				int pickUp = Integer.parseInt(data[6]);
				int drop = Integer.parseInt(data[7]);

				double distance;
				if(data.length > 8) 
				{
					distance = Double.parseDouble(data[8]);
				}
				else distance = 0;
				StopTimeVO nuevo = new StopTimeVO(id, arrivalTimeData, departureTimeData, stopId, stopSequence, stopSign, pickUp, drop, distance);
				StopVO h = stopTable.get(stopId);
				h.setTime(nuevo.getArrivTime());
				nuevo.setStop(h);
				tripsTable.get(id).addStopTime(nuevo);
				stopTimesTable.put(nuevo.getKey(), nuevo);
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {

		}
	}
	public void loadStops() 
	{
		try{
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/stops.txt")));
			reader.readLine();
			String line = reader.readLine();
			while(line != null){
				String data[] = line.split(",");

				int id = Integer.parseInt(data[0]);
				int code;
				if(data[1] != null && !data[1].equals(" ") && !data[1].isEmpty())
				{					
					code= Integer.parseInt(data[1]);
				}
				else code = 0;
				String name = data[2];
				String stopDesc = data[3];
				double stopLat = Double.parseDouble(data[4]);
				double stopLon = Double.parseDouble(data[5]);
				String stopZone = data[6];
				int locationType = Integer.parseInt(data[8]);
				StopVO nuevo = new StopVO(id, code, name, stopDesc, stopLat, stopLon, stopZone, null, locationType, null);
				stopTable.put( nuevo.getKey(), nuevo );
				line = reader.readLine();
			}
			reader.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public RedBlackBST<TripVO, Integer> tripsByStopAndHour(Integer stopId){
		RedBlackBST<TripVO, Integer> selectedTrips = new RedBlackBST<TripVO, Integer>();
		HashTable<StopTimeVO, String> middleSTimes = new HashTable.SeparateChainHash<StopTimeVO, String>(6007);
		
		StructuresIterator<StopTimeVO> sTimesValues = stopTimesTable.elementsIterator();
		
		for(; sTimesValues.getCurrent() != null; sTimesValues.next()){
			if(sTimesValues.getElement().getStopId() == stopId){
				middleSTimes.put(sTimesValues.getElement().getKey(), sTimesValues.getElement());
			}
		}
		
		sTimesValues = middleSTimes.elementsIterator();
		
		for(; sTimesValues.getCurrent() != null; sTimesValues.next()){
			TripVO search = tripsTable.get(sTimesValues.getElement().getTripId());
			selectedTrips.put(search.getArrivalTime(), search);
		}
		
		return selectedTrips;
	}
	
	public void printTrips(Integer stopId){
		RedBlackBST<TripVO, Integer> trips = tripsByStopAndHour(stopId);
		StructuresIterator<TripVO> iterTrips = trips.valuesInRange(trips.min(), trips.max());
		for(; iterTrips.getCurrent() != null; iterTrips.next()){
			TripVO actual = iterTrips.getElement();
			System.out.println("El viaje: " + actual.getKey() + " - de la ruta: " + actual.getRouteId() + " llega a la parada: - " + stopId + " - a las: " + actual.getarrt());
		}
	}
	
	public void stopsHourRange(Integer stopId, String initialHour, String finalHour){
		
		Integer hour1 = hour(initialHour);
		Integer hour2 = hour(finalHour);
		
		RedBlackBST<TripVO, Integer> tripsSelected = tripsByStopAndHour(stopId);
		StructuresIterator<TripVO> tripsInRange = tripsSelected.valuesInRange(hour1, hour2);
		
		for(; tripsInRange.getCurrent() != null; tripsInRange.next()){		
			TripVO actual = tripsInRange.getElement();
			System.out.println("El viaje: " + actual.getKey() + " - de la ruta: " + actual.getRouteId() + " llega a la parada: - " + stopId + " - a las: " + actual.getarrt());
		}
	}
	private int hour(String txt){
		String[] HMS = txt.split(":");
		String hour = HMS[0] + HMS[1] + HMS[2];
		return Integer.parseInt(hour);
	}
	
}

