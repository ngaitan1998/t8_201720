package src.model.data_structures.tests;

import junit.framework.TestCase;
import model.data_structures.Queue;
import model.exceptions.ElementNotFoundException;

public class QueueTest extends TestCase
{
	private Queue<Integer> numbers;

	public void setUp()
	{
		numbers = new Queue<Integer>();
	}
	public void setUp2( )
	{
		numbers = new Queue<Integer>();
		numbers.enqueue(0);
	}
	public void setUp3( )
	{
		numbers = new Queue<Integer>();

		for(int i = 0; i < 10; i++){
			numbers.enqueue(i);
		}
	}
	public void testEnqueue( )
	{
		setUp();
		numbers.enqueue(1);
		assertEquals("Deberian ser iguales", (Integer) 1, numbers.getFirst().getElement());

		numbers.enqueue(2);
		assertEquals("Deberian ser iguales", (Integer) 2, numbers.getLast().getElement());

		setUp3( );
		
		assertEquals("Deberian ser iguales", (Integer) 9, numbers.getLast().getElement());
	}
	public void testGet()
	{
		setUp( );
		try {
			numbers.getElementAtK(0);
			fail();
		} catch (ElementNotFoundException e) {
		}
		setUp2();
		try {
			numbers.getElementAtK(1);
			fail();
		} catch (ElementNotFoundException e) {
			// TODO: handle exception
		}
		setUp3();
		try {
			assertEquals( "Deberia encontrar el numero ", (Integer) 9, numbers.getElementAtK(0));
			assertEquals( "Deberia encontrar el numero ", (Integer) 0, numbers.getElementAtK(numbers.getSize() - 1));
			assertEquals( "Deberia encontrar el numero ", (Integer) 4, numbers.getElementAtK(5));
			assertEquals( "Deberia encontrar el numero ", (Integer) 1, numbers.getElementAtK(8));

		} catch (Exception e) {

		}
	}
	public void testGetSize()
	{
		setUp();
		int size = numbers.getSize();
		assertEquals("No deberia tener elementos", 0 , size);
		
		setUp2();
		size = numbers.getSize();
		assertEquals("El tamanio no es correcto", 1 , size);
		
		setUp3();
		size = numbers.getSize();
		assertEquals("El tamanio no es correcto", 10 , size);
	}
	public void testDequeue()
	{
		setUp();
		try {
			numbers.dequeue();
			fail();
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
		}
		
		setUp2();
		try {
			numbers.dequeue();
			assertNull( "Deberia estar vacia", numbers.getLast());
			assertNull( "Deberia estar vacia", numbers.getLast());

		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			numbers.dequeue();
			fail();
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
		}
		setUp3();
		try {
			numbers.dequeue();
			assertEquals("El primer elemento deberia de haber cambiado", ( Integer )1, numbers.getFirst().getElement());
			assertEquals("El ultimo elemento no deberia de haber cambiado", ( Integer )9, numbers.getLast().getElement());
		} catch (Exception e) 
		{
			// TODO: handle exception
		}
	}
}
