package src.model.data_structures.tests;

import junit.framework.TestCase;
import model.data_structures.RedBlackBST;
import model.data_structures.StructuresIterator;

public class RedBlackbstTest extends TestCase{
	
	
	RedBlackBST<ObjetoPrueba, Integer> redDoggo = new RedBlackBST<ObjetoPrueba, Integer>();
	
	ObjetoPrueba a = new ObjetoPrueba("perros", 10);
	ObjetoPrueba b = new ObjetoPrueba("gatos", 2);
	ObjetoPrueba c = new ObjetoPrueba("lobos", 123);
	ObjetoPrueba d = new ObjetoPrueba("zorros", 13);
	ObjetoPrueba e = new ObjetoPrueba("tigres", 14);
	ObjetoPrueba f = new ObjetoPrueba("ahhhhh", 1000);
	ObjetoPrueba g = new ObjetoPrueba("salmones", 1852);
	ObjetoPrueba h = new ObjetoPrueba("oso judio", 123);
	ObjetoPrueba i = new ObjetoPrueba("stop that", 3);
	
	public class ObjetoPrueba implements Comparable<ObjetoPrueba>{
		private String value;
		private Integer key;
		
		public ObjetoPrueba(String pValue, Integer pKey){
			this.value = pValue;
			this.key = pKey;
		}
		public Integer getKey(){
			return key;
		}
		public String getValue(){
			return value;
		}
		
		@Override
		public int compareTo(ObjetoPrueba obj) {
			int comp = getKey() - obj.getKey();
			if(comp > 0) return 1;
			else if (comp < 0) return -1;
			else return 0;
		}
	}
	
	void SetUp(){
		
		redDoggo.put(a.getKey(), a);
		redDoggo.put(b.getKey(), b);
		redDoggo.put(c.getKey(), c);
		redDoggo.put(d.getKey(), d);
		redDoggo.put(e.getKey(), e);
		redDoggo.put(f.getKey(), f);
		redDoggo.put(g.getKey(), g);
		redDoggo.put(h.getKey(), h);
		redDoggo.put(i.getKey(), i);
		
	}
	void SetUp2(){
		redDoggo.put(a.getKey(), a);

	
	}
	public void testIsEmpty(){
		assertEquals("El arbol deber�a estar vac�o", true, redDoggo.isEmpty());
		SetUp2();
		assertEquals("El arbol no deber�a estar vac�o", false, redDoggo.isEmpty());
	}
	
	public void testSize(){
		assertEquals("El tama�o no es correcto", 0, redDoggo.size());
		SetUp2();
		assertEquals("El tama�o no es correcto", 1, redDoggo.size());
		SetUp();
		assertEquals("El tama�o no es el correcto", 8, redDoggo.size());
	}
	public void testGet(){
		SetUp2();
		assertEquals("El objeto no es el esperado", a.getValue(), redDoggo.get(10).getValue());
		SetUp();
		assertEquals("El objeto no es el esperado", b.getValue(), redDoggo.get(2).getValue());
		assertEquals("El objeto no es el esperado", h.getValue(), redDoggo.get(123).getValue());
		assertEquals("El objeto no es el esperado", f.getValue(), redDoggo.get(1000).getValue());
	}
	
	public void testContains(){
		SetUp();
		assertEquals("El arbol deber�a contener este objeto", true, redDoggo.contains(a.getKey()));
		assertEquals("El arbol deber�a estar vacio", true, redDoggo.contains(a.getKey()));
		assertEquals("El arbol deber�a estar vacio", true, redDoggo.contains(b.getKey()));
		assertEquals("El arbol deber�a estar vacio", true, redDoggo.contains(e.getKey()));
		assertEquals("El arbol deber�a estar vacio", true, redDoggo.contains(f.getKey()));
		assertEquals("El arbol deber�a estar vacio", true, redDoggo.contains(g.getKey()));
		assertEquals("El arbol deber�a estar vacio", true, redDoggo.contains(h.getKey()));
		assertEquals("El arbol deber�a estar vacio", true, redDoggo.contains(i.getKey()));

	}
	
	public void testDeleteMin(){
		SetUp2();
		redDoggo.deleteMin();
		assertEquals("El arbol deber�a estar vacio", true, redDoggo.isEmpty());
		SetUp();
		try{
			redDoggo.deleteMin();	
			assertEquals("Este elemento no deber�a estar en el �rbol", false, redDoggo.contains(2));
		}
		catch(Exception e){
			//deber�a generar excepci�n
		}
	}
	public void testDeleteMax(){
		SetUp2();
		redDoggo.deleteMax();
		assertEquals("El arbol deber�a estar vacio", true, redDoggo.isEmpty());
		SetUp();
		try{
			redDoggo.deleteMax();	
			assertEquals("Este elemento no deber�a estar en el �rbol", false, redDoggo.contains(1852));
		}
		catch(Exception e){
			//deber�a generar excepci�n
 	}
		
	}
	public void testDelete(){
		SetUp2();
		redDoggo.delete(a.getKey());
		assertEquals("El arbol deber�a estar vac�o", true, redDoggo.isEmpty());
		SetUp();
		redDoggo.delete(c.getKey());
		try{
		assertEquals("El objeto no deber�a estar en el arbol", false, redDoggo.get(c.getKey()));
		}
		catch(Exception e){
			//deber�a generar excepci�n
			
		}
	}
	public void testHight(){
		SetUp2();
		assertEquals("La altura no es la esperada", 0, redDoggo.height());
		SetUp();
		assertEquals("La altura no es la esperada", 3, redDoggo.height());
	}
	public void testMin(){
		SetUp2();
		assertEquals("El valor m�nimo no es el esperado", redDoggo.min(), a.getKey());
		SetUp();
		assertEquals("El valor m�nimo no es el esperado", redDoggo.min(), b.getKey());
		redDoggo.deleteMin();
		assertEquals("El valor m�nimo no es el esperado", redDoggo.min(), i.getKey());
	}
	public void testMax(){
		SetUp2();
		assertEquals("El valor m�ximo no es el esperado", redDoggo.max(), a.getKey());
		SetUp();
		assertEquals("El valor m�ximo no es el esperado", redDoggo.max(), g.getKey());
		redDoggo.deleteMax();
		assertEquals("El valor m�ximo no es el esperado", redDoggo.max(), f.getKey());
	}
	public void testKeys(){
		SetUp();
		StructuresIterator<Integer> keys = redDoggo.keys();
		assertEquals("El iterador debe contener todos los elementos del arbol", true, keys.contains(a.getKey()));
		SetUp();
		StructuresIterator<Integer> keys1 = redDoggo.keys();
		assertEquals("El iterador debe contener todos los elementos del arbol", true, keys1.contains(b.getKey()));
		SetUp();
		StructuresIterator<Integer> keys2 = redDoggo.keys();
		assertEquals("El iterador debe contener todos los elementos del arbol", true, keys2.contains(d.getKey()));
		SetUp();
		StructuresIterator<Integer> keys3 = redDoggo.keys();
		assertEquals("El iterador debe contener todos los elementos del arbol", true, keys3.contains(e.getKey()));
		SetUp();
		StructuresIterator<Integer> keys4 = redDoggo.keys();
		assertEquals("El iterador debe contener todos los elementos del arbol", true, keys4.contains(f.getKey()));
		SetUp();
		StructuresIterator<Integer> keys5 = redDoggo.keys();
		assertEquals("El iterador debe contener todos los elementos del arbol", true, keys5.contains(g.getKey()));
		SetUp();
		StructuresIterator<Integer> keys6 = redDoggo.keys();
		assertEquals("El iterador debe contener todos los elementos del arbol", true, keys6.contains(h.getKey()));
		SetUp();
		StructuresIterator<Integer> keys7 = redDoggo.keys();
		assertEquals("El iterador debe contener todos los elementos del arbol", true, keys7.contains(i.getKey()));
	
	}
	
}
